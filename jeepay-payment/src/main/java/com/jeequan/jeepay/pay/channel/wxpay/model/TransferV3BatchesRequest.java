package com.jeequan.jeepay.pay.channel.wxpay.model;

import com.github.binarywang.wxpay.bean.transfer.TransferBatchesRequest;
import com.google.gson.annotations.SerializedName;

/**
 * 微信v3转账增加异步通知参数
 * @ClassName : TransferV3BatchesRequest
 * @Author : tostyle
 * @Date: 2024-05-08 10:54
 */
public class TransferV3BatchesRequest extends TransferBatchesRequest {

    @SerializedName("notify_url")
    private String notifyUrl;

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }
}
